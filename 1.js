let paragraph = document.getElementsByTagName('p');
// elem.style.background = 'red';
for (let i = 0; i < paragraph.length; i++){
    paragraph[i].style.backgroundColor = 'red'
}



let optionsListElement = document.getElementById('optionsList');
console.log(optionsListElement);



let parentElement = optionsListElement.parentElement;
console.log(parentElement);



let childNodes = optionsListElement.childNodes;
for (let i = 0; i < childNodes.length; i++) {
    let childNode = childNodes[i];
    console.log('Дочірній елемент:', childNode.nodeName, childNode.nodeType);
};



let elemClass = document.getElementById('testParagraph');
elemClass.innerText = 'This is a paragraph';



let mainHeader = document.querySelector('.main-header');
let childElements = mainHeader.children;
console.log(childElements);
for (let i = 0; i < childElements.length; i++) {
    childElements[i].classList.add('nav-item');
}
console.log('Елементи "nav-item":', childElements);


let newElem = document.getElementsByClassName("section-title")
for (let i = 0; i < newElem.length; i++) {
    newElem[i].classList.remove("section-title");
}


// 1.Це модель документа, яка представляє весь вміст сторінки у вигляді об'єктів, які можна змінювати. Кожен HTML-тег є об'єктом, 
// вкладені теги є дітьми батьківського елемента.Текст, який знаходиться всередині тега, також є об'єктом.
// 2.innerHTML представляє HTML-вміст елемента, включаючи всі вкладені теги та їх атрибути,зміни в innerHTML впливають на структуру 
// та вміст елемента, а не лише на текст.А innerText представляє лише видимий текст елемента, ігноруючи всі HTML-теги, отримує лише текст.
// 3.Звернутися до елемента можна за допомогою getElementById або getElementsByClassName або getElementsByTagName, 
// але більш функціональним вважаеться querySelector або querySelectorAll оскільки дозволяють використовувати CSS-селектори для вибору елементів.